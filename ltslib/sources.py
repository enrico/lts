from __future__ import annotations

import contextlib
import dbm
import gzip
import logging
import os
from typing import Dict, Generator

from debian import deb822
import requests

from ltslib.pickledict import PickleDict

log = logging.getLogger(os.path.basename(__file__))


@contextlib.contextmanager
def sources(dist: str, cachedir: str) -> Generator[Dict[str, Dict[str, str]], None, None]:
    """
    Get a dict-like object associating source package names with dicts with the
    parsed Sources file stanzas
    """
    cache = os.path.join(cachedir, f"Sources-{dist}.cache")
    if not os.path.exists(cache + ".db"):
        url = f"https://deb.debian.org/debian/dists/{dist}/main/source/Sources.gz"
        log.info("Fetch and index sources info for %s from %s", dist, url)
        with dbm.open(cache, "n") as _db:
            db = PickleDict(_db)
            res = requests.get(url, stream=True, allow_redirects=True)
            res.raise_for_status()
            with gzip.open(res.raw, "rt") as fd:
                data = fd.read()
                for stanza in deb822.Sources.iter_paragraphs(data):
                    db[stanza["package"]] = stanza
            yield db
    else:
        with dbm.open(cache, "r") as _db:
            yield PickleDict(_db)
