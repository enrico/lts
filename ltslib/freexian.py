from __future__ import annotations

import re
from typing import Dict


def packages_to_support(pathname: str) -> Dict[str, float]:
    """
    Parse packages-to-support file
    """
    res: Dict[str, float] = {}
    re_line = re.compile(r"^(\S+)\s+\(([0-9.]+)\s*%\)")
    with open(pathname, "rt") as fd:
        for lineno, line in enumerate(fd, start=1):
            if (mo := re_line.match(line)):
                res[mo.group(1)] = float(mo.group(2))
            else:
                raise RuntimeError(f"{pathname}:{lineno}: unparsable line {line!r}")
    return res
