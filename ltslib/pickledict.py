from __future__ import annotations

import collections.abc
import pickle
from typing import Dict, Any


class PickleDict(collections.abc.MutableMapping):
    """
    Dict-like wrapper that transparently pickles/unpickles values
    """

    def __init__(self, db: Dict[str, bytes]):
        self.db = db

    def __getitem__(self, key: str) -> Any:
        res = self.db.get(key)
        if res is None:
            return None
        return pickle.loads(res)

    def __setitem__(self, key: str, value: Any):
        self.db[key] = pickle.dumps(value)

    def __delitem__(self, key: str):
        del self.db[key]

    def __len__(self):
        return len(self.db)

    def __iter__(self):
        return self.db.__iter__()

    def items(self):
        return (
            (k, pickle.loads(v))
            for k, v in self.db.items())

    def values(self):
        return (pickle.loads(v) for v in self.db.values())
