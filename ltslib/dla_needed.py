from __future__ import annotations

import datetime
import os
import re
from typing import List, NamedTuple, Optional, Union

import git


class Note(NamedTuple):
    date: Union[datetime.date, str]
    text: str


class Entry:
    """
    An entry in dla-needed.txt
    """
    re_head = re.compile(r"^(\S+)(?:\s+\(([^)]+)\))?\s*$")

    def __init__(self, lineno: int, lines: List[str]):
        # Line at which the entry starts
        self.lineno = lineno
        # Text lines that make up the entry
        self.lines = lines

    @property
    def name(self) -> str:
        """
        Return the package name
        """
        mo = self.re_head.match(self.lines[0])
        if not mo:
            raise RuntimeError(f"{self.lineno}:unparsable package line")
        return mo.group(1)

    @property
    def claimed_by(self) -> Optional[str]:
        """
        Return the name of the person who claimed this package, or None if it
        is free to take
        """
        mo = self.re_head.match(self.lines[0])
        if not mo:
            raise RuntimeError(f"{self.lineno}:unparsable package line")
        if (name := mo.group(2)):
            return name
        else:
            return None

    @property
    def notes(self) -> List[Note]:
        """
        Return the annotations for thsi package
        """
        notes: List[Note] = []
        for line in self.lines[1:]:
            if (mo := re.match(r"\s+NOTE: (\d+): (.+)", line)):
                try:
                    dt = datetime.datetime.strptime(mo.group(1), "%Y%m%d")
                except ValueError:
                    dt = mo.group(1)
                notes.append(Note(dt, mo.group(2)))
        return notes

    def claim(self, name: str):
        """
        Assign the entry
        """
        self.lines[0] = f"{self.name} ({name})"

    def unclaim(self):
        """
        Unassign the entry
        """
        self.lines[0] = self.name


class DLANeeded:
    """
    Interface to a dla-needed.txt
    """
    def __init__(self, pathname: str):
        self.pathname = pathname
        self.intro: List[str] = []
        self.entries: List[Entry] = []
        self.load()

    def get(self, name: str) -> Optional[Entry]:
        """
        Return the entry for a package
        """
        for e in self.entries:
            if e.name == name:
                return e
        return None

    def save(self):
        """
        Write out the contents to dla-needed.txt
        """
        with open(self.pathname, "wt") as fd:
            for line in self.intro:
                print(line, file=fd)
            for entry in self.entries:
                print("--", file=fd)
                for line in entry.lines:
                    print(line, file=fd)
            print("--", file=fd)
            fd.flush()
            os.fsync(fd.fileno())

    def load(self):
        """
        Parse and load the contents of dla-needed.txt
        """
        # Read the file and split stanzas
        in_intro = True
        stanza_lines: List[str] = []
        with open(self.pathname, "rt") as fd:
            for lineno, line in enumerate(fd):
                line = line.rstrip()
                if line == "--":
                    if in_intro:
                        in_intro = False
                    elif stanza_lines:
                        self.entries.append(Entry(lineno, stanza_lines))
                        stanza_lines = []
                else:
                    if in_intro:
                        self.intro.append(line)
                    else:
                        stanza_lines.append(line)

        if stanza_lines:
            self.entries.append(Entry(stanza_lines))

    def push(self, message: str):
        """
        Save the file, commit it with the given message, and push
        """
        self.save()
        repo_path = os.path.abspath(os.path.join(os.path.dirname(self.pathname), ".."))
        repo = git.Repo(repo_path)
        repo.index.add(self.pathname)
        repo.index.commit(message)
        origin = repo.remote("origin")
        origin.push()
