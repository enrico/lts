from __future__ import annotations

import contextlib
import getpass
import logging
import os
import subprocess
import time
from typing import Generator, NamedTuple, Optional

from transilience import Host, Playbook, role
from transilience.actions import builtin

log = logging.getLogger(os.path.basename(__file__))


class ServerInfo(NamedTuple):
    """
    Information about an hcloud server
    """
    name: str
    ipv6: str
    ipv4: str

    def get_ipv4_address(self) -> str:
        """
        Get the IPv4 server address
        """
        return self.ipv4

    def get_ipv6_address(self) -> str:
        """
        Get the IPv6 server address
        """
        return self.ipv6.split("/")[0] + "1"

    def get_address(self) -> str:
        """
        Get the server address to use to log in
        """
        return self.get_ipv4_address()


class BuildServer(role.Role):
    """
    Buildserver role for provisioning
    """
    def start(self):
        username = getpass.getuser()

        self.add(builtin.apt(
            update_cache=True,
            cache_valid_time=3600,
        ), name="update package database",
           # Run only if the previous copy changed anything
           # when={a: ResultState.CHANGED},
        )

        self.add(builtin.apt(
            name=["systemd-container", "vim", "mc", "zstd"],
            state="present",
        ), name="install host system packages")

        self.add(builtin.user(
            name=username,
            comment=username,
            uid=1000,
        ))


class Provision(Playbook):
    """
    Provision a build server
    """
    def __init__(self, server: ServerInfo):
        super().__init__()
        self.server = server

    def start(self, host: Host):
        self.add_role(BuildServer)


class HCloud:
    """
    Manage a build machine on hetzner
    """
    def __init__(self, name: str = "build", type: str = "ccx22", image: str = "debian-11", context="freexian"):
        self.context = context
        self.name = name
        self.type = type
        self.image = image
        subprocess.run(["hcloud", "context", "use", "freexian"], check=True)

    def list_servers(self) -> Generator[ServerInfo, None, None]:
        """
        List existing servers
        """
        res = subprocess.run([
                "hcloud", "server", "list", "-o", "noheader", "-o", "columns=name,ipv6,ipv4"
            ], check=True, text=True, stdout=subprocess.PIPE)
        for line in res.stdout.splitlines():
            yield ServerInfo(*line.split())

    def find_server(self) -> Optional[ServerInfo]:
        """
        Check if the server we want exists
        """
        for srv in self.list_servers():
            if srv.name == self.name:
                return srv
        return None

    def start_server(self) -> Optional[ServerInfo]:
        """
        Start the server
        """
        log.info("hcloud %s: starting server", self.name)
        create_cmd = [
            "hcloud", "server", "create",
            "--start-after-create",
            "--name", self.name,
            "--type", self.type,
            "--image", self.image,
        ]

        # List available ssh keys for this Hetzner project, and add them all
        res = subprocess.run(
                ["hcloud", "ssh-key", "list", "-o", "noheader", "-o", "columns=name"],
                check=True, text=True, stdout=subprocess.PIPE)
        for key in res.stdout.split():
            create_cmd.extend(("--ssh-key", key))

        subprocess.run(create_cmd, check=True)

        while True:
            res = self.find_server()
            if res is not None:
                break
            time.sleep(0.5)

        # Remove an old host key: since we just recreated the server, its ssh host key has changed
        for addr in (res.get_ipv6_address(), res.get_ipv4_address()):
            subprocess.run([
                "ssh-keygen", "-f", os.path.expanduser("~/.ssh/known_hosts"), "-R", addr],
                check=False)

        return res

    def stop_server(self):
        subprocess.run(
            ["hcloud", "server", "delete", self.name],
            check=True)

    @contextlib.contextmanager
    def ssh_socket(self, server: ServerInfo):
        """
        Open a ssh control socket and keep it open for the duration of this
        context manager
        """
        addr = server.get_address()
        log.info("hcloud %s: opening ssh socket to root@%s", self.name, addr)
        subprocess.run(["ssh", "-MNf", "-o", "StrictHostKeyChecking=no", f"root@{addr}"], check=True)
        try:
            yield
        finally:
            subprocess.run(["ssh", "-O", "exit", f"root@{addr}"], check=True)

    def login(self, server: ServerInfo):
        """
        Log into the server
        """
        addr = server.get_address()
        log.info("hcloud %s: logging into root@%s", self.name, addr)
        res = subprocess.run(["ssh", f"root@{addr}"], check=False)
        return res.returncode

    def provision(self, server: ServerInfo):
        """
        Provision the server
        """
        log.info("hcloud %s: provisioning root@%s", self.name, server.get_address())
        host = Host(name=server.name, args={
            "method": "ssh",
            "hostname": server.get_address(),
            "username": "root",
            "ssh_args": ["-vv"],
            "compression": False,
        })

        prov = Provision(server)
        prov.provision([host], check_mode=False)

    def send_os_image(self, server: ServerInfo, image: str):
        """
        Send an OS image from /var/lib/machines to /srv
        """
        res = subprocess.run(["ssh", f"root@{server.get_address()}", "test", "-d", f"/srv/{image}"])
        if res.returncode == 0:
            return

        log.info("hcloud %s: sending %r os image to root@%s", self.name, image, server.get_address())

        p1 = subprocess.Popen(
                ["sudo", "tar", "-C", "/var/lib/machines/", "--zstd", "-cf", "-", image],
                stdout=subprocess.PIPE)
        p2 = subprocess.Popen(
                ["ssh", f"root@{server.get_address()}", "tar", "-C", "/srv", "--zstd", "-xf", "-"],
                stdin=p1.stdout)
        p1.stdout.close()
        p1.wait()
        p2.wait()
