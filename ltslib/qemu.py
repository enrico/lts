from __future__ import annotations

import contextlib
import hashlib
import logging
import os
import subprocess
import sys
import tempfile
import uuid
from typing import Generator, Optional

import libvirt

log = logging.getLogger(os.path.basename(__file__))

VIRT_BUILDER_ALIASES = {
    "stretch": "debian-9",
}


def get_uuid(name: str):
    return str(uuid.uuid3(uuid.NAMESPACE_URL, "httpd://debian.org/lts/playground/" + name))


class QEmu:
    """
    Manage ephemeral qemu virtual machines
    """
    def __init__(self, cachedir: str, dist: str = "stretch"):
        # URI to connect to libvirt
        self.libvirt_uri = "qemu:///system"

        # Directory where disk images are stored
        self.image_dir = os.path.abspath(os.path.join(cachedir, "images"))

        # Distribution to use
        self.dist = dist

        # ssh public key to use to connect to the machine
        # (autodetected from user's public key)
        self.ssh_public_key = None
        for path in ("~/.ssh/id_rsa.pub",):
            path = os.path.expanduser(path)
            if os.path.exists(path):
                self.ssh_public_key = path
                break

    @contextlib.contextmanager
    def connection(self):
        """
        Start/end a libvirt connection
        """
        conn = libvirt.open(self.libvirt_uri)
        try:
            yield conn
        finally:
            conn.close()

    def lookup_network(self, connection, name: str) -> Optional[str]:
        """
        Lookup a virNetwork object for the test network.
        """
        # Iterate networks to prevent a spurious message on stderr if
        # networkLookupByName fails
        if name not in connection.listNetworks():
            return None

        try:
            return connection.networkLookupByName(name)
        except libvirt.libvirtError as e:
            if str(e).startswith("Network not found:"):
                return None
            raise

    def start_network(self, connection, name: str = "default"):
        """
        Activate and return the virNetwork object for this system
        """
        # sudo virsh net-start default  (might need to be done by hand if it says 'Network default not found')
        network = self.lookup_network(connection, name)
        if network is None:
            raise RuntimeError(f"Network {name} not found, you may need to run `sudo virsh net-start {name}`")

        if not network.isActive():
            # virsh -c qemu:///system net-start $name
            log.info("start network %s", name)
            network.create()

        return network

    @contextlib.contextmanager
    def firstboot_script(self) -> Generator[Optional[str], None, None]:
        """
        If needed for the current distro, return the pathname for a firstboot
        script to use during main image setup
        """
        if self.dist == "stretch":
            with tempfile.NamedTemporaryFile(mode="wt") as fd:
                fd.write("""#!/bin/sh
# Fixup a newly installed debian stretch in libvirt

# Fix network interface name
sed -i -re 's/ens2/enp1s0/' /etc/network/interfaces

# Bring up the network
ifup enp1s0

# Set a random ssh host key
ssh-keygen -f /etc/ssh/ssh_host_rsa_key -N '' -t rsa
systemctl restart sshd

# Run a round of updates
apt update
apt -y upgrade
""")
                fd.flush()
                os.fsync(fd.fileno())
                yield fd.name
        else:
            yield None

    def get_main_image_path(self) -> str:
        """
        Get the pathname for the .qcow2 file with the main disk image
        """
        pathname = os.path.join(self.image_dir, f"{self.dist}-main.qcow2")
        if not os.path.exists(pathname):
            log.info("%s: building main image", self.dist)

            os.makedirs(self.image_dir, exist_ok=True)

            cmd = [
                "virt-builder", VIRT_BUILDER_ALIASES.get(self.dist, self.dist),
                "--format", "qcow2",
                "-o", pathname,
                "--root-password", "password:root",
            ]

            with self.firstboot_script() as script:
                if script is not None:
                    cmd += ["--firstboot", script]

                if self.ssh_public_key:
                    log.info(" using ssh public key %s", self.ssh_public_key)
                    cmd += ["--ssh-inject", "root:file:" + self.ssh_public_key]

                subprocess.run(cmd, check=True)

        return pathname

    def get_image(self, name: str) -> str:
        """
        Get the pathname for a .qcow2 file with a copy-on-write writable
        version of the main disk image
        """
        if name == "main":
            raise RuntimeError("'main' is reserved for the readonly main image name")
        pathname = os.path.join(self.image_dir, f"{self.dist}-{name}.qcow2")
        if not os.path.exists(pathname):
            cmd = ["qemu-img", "create", "-f", "qcow2", '-F', 'qcow2',
                   "-o", "backing_file=" + self.get_main_image_path(),
                   pathname]
            subprocess.run(cmd, stdout=subprocess.DEVNULL, check=True)
        return pathname

    def lookup_domain(self, connection, name: str):
        """
        Lookup a domain object with the given name
        """
        # Iterate domains to prevent a spurious message on stderr if
        # lookupByName fails
        for domain in connection.listAllDomains():
            if domain.name() == name:
                return domain
        return None

    def start_machine(self, connection, name: str, network_name: str = "default"):
        """
        Start a machine with the given name
        """
        domain = self.lookup_domain(connection, name)
        if domain is None:
            uuid = get_uuid(name)

            image = self.get_image(name)

            # Build a mac address based on the machine name. No particular security
            # is required from this hashing, it's just to tie mac addresses to
            # machine names.
            digest = hashlib.sha1(name.encode()).hexdigest()
            mac = "52:54:00:{}:{}:{}".format(digest[0:2], digest[2:4], digest[4:6])

            workdir = os.path.abspath(os.path.dirname(sys.argv[0]))

            self.start_network(connection, network_name)

            log.info("Defining domain %s", name)
            xml_cfg = f"""<domain type="kvm">
  <name>{name}</name>
  <uuid>{uuid}</uuid>
  <metadata>
    <libosinfo:libosinfo xmlns:libosinfo="http://libosinfo.org/xmlns/libvirt/domain/1.0">
      <libosinfo:os id="http://debian.org/debian/10"/>
    </libosinfo:libosinfo>
  </metadata>
  <memory>8388608</memory>
  <currentMemory>8388608</currentMemory>
  <vcpu>2</vcpu>
  <os>
    <type arch="x86_64" machine="q35">hvm</type>
    <boot dev="hd"/>
  </os>
  <features>
    <acpi/>
    <apic/>
  </features>
  <cpu mode="host-model"/>
  <clock offset="utc">
    <timer name="rtc" tickpolicy="catchup"/>
    <timer name="pit" tickpolicy="delay"/>
    <timer name="hpet" present="no"/>
  </clock>
  <pm>
    <suspend-to-mem enabled="no"/>
    <suspend-to-disk enabled="no"/>
  </pm>
  <devices>
    <emulator>/usr/bin/qemu-system-x86_64</emulator>
    <disk type="file" device="disk">
      <driver name="qemu" type="qcow2" cache="unsafe"/>
      <source file="{image}"/>
      <target dev="vda" bus="virtio"/>
    </disk>
    <controller type="usb" index="0" model="qemu-xhci" ports="15"/>
    <interface type="network">
      <source network="{network_name}"/>
      <mac address="{mac}"/>
      <model type="virtio"/>
    </interface>
    <console type="pty"/>
    <channel type="unix">
      <source mode="bind"/>
      <target type="virtio" name="org.qemu.guest_agent.0"/>
    </channel>
    <input type="tablet" bus="usb"/>
    <graphics type="vnc" port="-1"/>
    <video>
      <model type="qxl"/>
    </video>
    <rng model="virtio">
      <backend model="random">/dev/urandom</backend>
    </rng>
    <filesystem type="mount" accessmode="mapped">
      <source dir="{workdir}"/>
      <target dir="/srv/freexian"/>
      <readonly/>
      <address type="pci" domain="0x0000" bus="0x07" slot="0x00" function="0x0"/>
    </filesystem>
  </devices>
</domain>
"""
            domain = connection.defineXML(xml_cfg)

        if not domain.isActive():
            domain.create()

        return domain

    def stop_machine(self, connection, name: str):
        """
        Stop a machine with the given name
        """
        domain = self.lookup_domain(connection, name)
        if domain is not None:
            if domain.isActive():
                log.info("Halting existing domain %s", name)
                domain.destroy()
            log.info("Undefining existing domain %s", name)
            domain.undefine()
